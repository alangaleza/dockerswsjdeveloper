//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.20 at 01:57:18 PM CEST 
//


package pl.nbp.soa.xsd.uslugi.svc_sws.odmtacceptance;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pl.nbp.soa.xsd.uslugi.svc_sws.odmtacceptance package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pl.nbp.soa.xsd.uslugi.svc_sws.odmtacceptance
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OdMtAcceptanceInMessage }
     * 
     */
    public OdMtAcceptanceInMessage createOdMtAcceptanceInMessage() {
        return new OdMtAcceptanceInMessage();
    }

    /**
     * Create an instance of {@link Request }
     * 
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link OdMtAcceptanceOutMessage }
     * 
     */
    public OdMtAcceptanceOutMessage createOdMtAcceptanceOutMessage() {
        return new OdMtAcceptanceOutMessage();
    }

}
