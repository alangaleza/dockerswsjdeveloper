package com.sws.service;

import org.apache.logging.log4j.util.Strings;

import org.springframework.stereotype.Service;

import pl.nbp.sws.pmcorrectionrequest.model.PmCorrectionRequestInMessage;
import pl.nbp.sws.pmcorrectionrequest.model.PmCorrectionRequestOutMessage;
import pl.nbp.sws.pmcorrectionrequest.model.Request;
import pl.nbp.sws.xsd.modeledomenowe.sws.Response;

@Service
public class ZadanieKorektyZapotrzebowaniaSWSService {

    public PmCorrectionRequestOutMessage prepareResponse(PmCorrectionRequestInMessage request) {
        Request req = request.getRequest();
        Response res= new Response();
        if (Strings.isBlank(req.getIdPobraniaSWS()) || req.getIdDokumentuEOD() == 0) {
            res.setKomunikatBledu("ZadanieKorektyZapotrzebowaniaSWS.komunikatBledu: Wymagane pole nie jest ustawione.");
        } else {
            res.setIdDokumentuEod(-200L);
            res.setKodPowrotu("-200");
        }

        PmCorrectionRequestOutMessage response = new PmCorrectionRequestOutMessage();
        response.setResponse(res);
        return response;
    }
}
