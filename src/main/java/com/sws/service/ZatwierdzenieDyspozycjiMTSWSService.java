package com.sws.service;

import org.springframework.stereotype.Service;

import pl.nbp.sws.odmtacceptance.model.OdMtAcceptanceInMessage;
import pl.nbp.sws.odmtacceptance.model.OdMtAcceptanceOutMessage;
import pl.nbp.sws.odmtacceptance.model.Request;
import pl.nbp.sws.xsd.modeledomenowe.sws.Response;

@Service
public class ZatwierdzenieDyspozycjiMTSWSService {

    public OdMtAcceptanceOutMessage prepareResponse(OdMtAcceptanceInMessage request) {
        Request req = request.getRequest();
        Response res = new Response();
        System.out.println("WCHODZI");
        System.out.println(request.getParWywolania().getUzytkownik());
        System.out.println(request.getParWywolania().getUzytkownikLogin());
        System.out.println(request.getParWywolania().getData());
        System.out.println(request.getRequest().getIdDokumentuEod());
        System.out.println(request.getRequest().getIdOdMtSWS());
        System.out.println(request.getRequest().getNumerDyspozycjiMTSWS());
        
        if (req.getIdOdMtSWS() == null || req.getIdDokumentuEod() == 0) {
            res.setKodPowrotu("-1");
            res.setKomunikatBledu("ZatwierdzenieDyspozycjiMTSWS.komunikatBledu: Wymagane pole nie jest ustawione.");
        } else {
            System.out.println("W ELSIE");
            res.setIdDokumentuEod(request.getRequest().getIdDokumentuEod());
            res.setKodPowrotu("1");
        }

        OdMtAcceptanceOutMessage response = new OdMtAcceptanceOutMessage();
        response.setResponse(res);
        return response;
    }
}
